﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace tabbed
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
            TabbedPage tabbed = new TabbedPage();
            Children.Add(new Page1());
            Children.Add(new Page2());
            Children.Add(new Page3());

        }

        private async void TabbedPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(500);
            home.Icon = "home2.png";
        }

        private async void Home_Disappearing(object sender, EventArgs e)
        {
            await DisplayAlert("Hey", "You better have picked Playstation.", "OK");
            home.Icon = "home.png";
        }
    }
}
