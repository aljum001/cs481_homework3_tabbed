﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
		public Page3 ()
		{
			InitializeComponent ();
		}
        void ChangeImg1(Button Sender, EventArgs e)
        {
            Img.Source = "Nswitch.jpg";
            B1.BackgroundColor = Color.Blue;
        }

        void ChangeImg2(Button Sender, EventArgs e)
        {
            Img.Source = "Nswitch2.jpg";
            B2.BackgroundColor = Color.Blue;
        }

        void ChangeImg3(Button Sender, EventArgs e)
        {
            Img.Source = "trash.jpg";
            B3.BackgroundColor = Color.Blue;
            B3.Text = "Haha!";
        }

        private async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(500);
            Nintendo.Icon = "nincon.png";
            B3.Text = "Don't Click";
            B1.BackgroundColor = Color.Red;
            B2.BackgroundColor = Color.Red;
            B3.BackgroundColor = Color.Red;
            await Task.Delay(200);
            await B1.RelRotateTo(360);
            await B2.RelRotateTo(360);
            await B3.RelRotateTo(360);
        }

        private async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            await DisplayAlert("Phew", "Aren't you glad you left?", "OK");
            Nintendo.Icon = "Nicon.png";
            B1.BackgroundColor = Color.Red;
            B2.BackgroundColor = Color.Red;
        }
    }
}