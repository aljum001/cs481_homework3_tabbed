﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading;

namespace tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();

        }
        void BGSlider(object sender, ValueChangedEventArgs args)
        {
            L1.TextColor = Color.White;
            double v = args.NewValue;
            int value = Convert.ToInt32(v);
            if (value == 0)
            {
                pic.Source = "PSBLUE.jpg";
                colorSlider.BackgroundColor = Color.Blue;
            }
            else if (value == 1)
            {
                pic.Source = "PSPINK.jpg";
                colorSlider.BackgroundColor = Color.Pink;
            }
            else if (value == 2)
            {
                pic.Source = "PSRED.jpg";
                colorSlider.BackgroundColor = Color.Red;
            }
            else if (value == 3)
            {
                pic.Source = "PSGOLD.jpg";
                colorSlider.BackgroundColor = Color.Gold;
            }
            else if (value == 4)
            {
                pic.Source = "PSPURPLE.jpg";
                colorSlider.BackgroundColor = Color.Purple;
            }
            else if (value == 5)
            {
                pic.Source = "PSCAMO.jpg";
                colorSlider.BackgroundColor = Color.DarkKhaki;
            }
        }
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(500);
            PS.Icon = "ps2.png";
            pic.Source = "ps4.png";
            colorSlider.BackgroundColor = Color.Black;
            await Task.Delay(500);
            await pic.RelRotateTo(360);
            L1.Text = "Use the slider below to change the background color";
            
        }

        private async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            await DisplayAlert("Wow", "Why would you leave, there aren't better option...", "OK");
            PS.Icon = "PSicon.png";
        }
    }
}